# BIDS Educational Slides

## Compilation Instructions

```sh
pdflatex -shell-escape pres.tex && bibtex pres && pdflatex -shell-escape pres.tex && pdflatex -shell-escape pres.tex
```
